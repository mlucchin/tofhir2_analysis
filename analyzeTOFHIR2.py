from root_pandas import read_root
import ROOT
from ROOT import TCanvas, TGraphErrors, TH1F, TH2F, TLegend, gStyle, gPad, TF1, TFile, TTree, TProfile
from ROOT import kGreen, kRed, kYellow, kBlue, kViolet, kCyan, kBlack, kOrange, kMagenta, kRainBow, kCopper, kSolar, kTemperatureMap, kOcean, kDarkBodyRadiator, kDeepSea
import sys
import numpy as np
import pandas as pd

from tofhir_analysis_func import plotTots, plotTimeRes, plotTimeResAmpCorr




gStyle.SetTitleXOffset (1.00) ;                                                                                        
gStyle.SetTitleYOffset (1.2) ;                                                                                                                                                                                                                 
gStyle.SetPadLeftMargin (0.13) ;                                                                                       
gStyle.SetPadBottomMargin (0.13) ;                                                                                                                                                                                                              
gStyle.SetTitleSize (0.05, "xyz") ;                                                                                    
gStyle.SetLabelSize (0.035,"xyz") ;  
    
gStyle.SetLegendBorderSize(0);
gStyle.SetLegendFillColor(0);
gStyle.SetLegendFont(42);
gStyle.SetLegendTextSize(0.035);

mycolors=[kBlack, kRed+1, kBlue+1, kGreen+1, kYellow+1, kOrange+1, kViolet+1, kCyan+1,
                  kRed+2, kBlue+2, kGreen+2, kYellow+2, kOrange+2, kViolet+2, kCyan+2,
                  kRed,   kBlue,   kGreen,   kYellow,   kOrange,   kViolet,   kCyan]

for i in range(100):
    mycolors.append(kRainBow+i)

cols = ['step1','step2', 
        'tot1', 'time1', 'channelID1', 'energy1', 
        'tot2', 'time2', 'channelID2', 'energy2']


data_path = "../data/tofhir2/reco/"
filename = str(data_path+"run"+sys.argv[1]+"_c.root" )
               
print("reading file: ", filename)

df = read_root(filename, 'data', columns=cols)

#sample
#df_train     = df.sample(frac=0.5)
maxEvents = 500000
if (len(df)>maxEvents):
    df = df.sample(n=maxEvents)
    
df.index = range(len(df))
#df = df[df.step1==2]
#print (df)
#print("first selection applied")



config_list = []
for i in range(len(df)):         
    if ([df.step1[i], df.step2[i]] not in config_list):
        config_list.append([df.step1[i],df.step2[i]])


print ("configurations found in data: ")
print (config_list)


df_list = []
for config in config_list:    
    df_config = df[(df.step1==config[0]) & (df.step2 ==config[1])]
    df_config.index = range(len(df_config))
    print("length of subset df_config: ", len(df_config))
    df_list.append(df_config)





print("first cycle to find tot")

results = plotTots(df_list,config_list)
hTot1_all = results[0]
hTot2_all = results[1]
hTotRatio_all = results[2]
hTotScatter_all = results[3]

vals_tot1 = results[4]
vals_tot2 = results[5]
    


cTots1 = TCanvas ("cTots1", "cTots1", 600, 500)
hTot1_all[0][2].Draw()
hTot1_all[0][2].GetYaxis().SetRangeUser(1, hTot1_all[0][2].GetMaximum()*5)
hTot1_all[0][2].SetStats(0)
hTot1_all[0][2].GetXaxis().SetTitle("tot1 [ns]")
hTot1_all[0][2].GetYaxis().SetTitle("Counts")
legConfig = TLegend(0.15, 0.6, 0.5, 0.88)
for i in range(len(hTot1_all)):
    hTot1 = hTot1_all[i]
    hTot1[2].SetLineColor(mycolors[i])
    hTot1[2].Draw("same")
    label = str("OV = "+str(hTot1[0])+ ", vth_{1} = "+str(hTot1[1]))
    legConfig.AddEntry(hTot1[2], label, "lpe")
    
legConfig.Draw()    
gPad.SetLogy()
cTots1.Update()


cTots2 = TCanvas ("cTots2", "cTots2", 600, 500)
hTot2_all[0][2].Draw()
hTot2_all[0][2].GetYaxis().SetRangeUser(1, hTot2_all[0][2].GetMaximum()*5)
hTot2_all[0][2].SetStats(0)
hTot2_all[0][2].GetXaxis().SetTitle("tot2 [ns]")
hTot2_all[0][2].GetYaxis().SetTitle("Counts")
legConfig = TLegend(0.15, 0.6, 0.5, 0.88)
for i in range(len(hTot2_all)):
    hTot2 = hTot2_all[i]
    hTot2[2].SetLineColor(mycolors[i])
    hTot2[2].Draw("same")
    label = str("OV = "+str(hTot2[0])+ ", vth_{1} = "+str(hTot2[1]))
    legConfig.AddEntry(hTot2[2], label, "lpe")
    
legConfig.Draw()    
gPad.SetLogy()
cTots2.Update()


cTotsRatio = TCanvas ("cTotsRatio", "cTotsRatio", 600, 500)
hTotRatio_all[0][2].Draw()
hTotRatio_all[0][2].GetYaxis().SetRangeUser(1, hTotRatio_all[0][2].GetMaximum()*5)
hTotRatio_all[0][2].SetStats(0)
hTotRatio_all[0][2].GetXaxis().SetTitle("tot1/tot2")
hTotRatio_all[0][2].GetYaxis().SetTitle("Counts")
legConfig = TLegend(0.15, 0.6, 0.5, 0.88)
for i in range(len(hTotRatio_all)):
    hTot1 = hTotRatio_all[i]
    hTot1[2].SetLineColor(mycolors[i])
    hTot1[2].Draw("same")
    label = str("OV = "+str(hTot1[0])+ ", vth_{1} = "+str(hTot1[1]))
    legConfig.AddEntry(hTot1[2], label, "lpe")
    
legConfig.Draw()    
gPad.SetLogy()
cTotsRatio.Update()


cTotsScatter = TCanvas ("cTotsScatter", "cTotsScatter", 1500, 500)
cTotsScatter.Divide(len(config_list),1)
for i in range(len(config_list)):
    cTotsScatter.cd(i+1)
    
    hTotScatter_all[i][2].Draw("COLZ")
    hTotScatter_all[i][2].SetStats(0)
    hTotScatter_all[i][2].GetXaxis().SetTitle("tot1 [ns]")
    hTotScatter_all[i][2].GetYaxis().SetTitle("tot2 [ns]")
    
    label = str("OV = "+str(hTotScatter_all[i][0])+ ", vth_{1} = "+str(hTotScatter_all[i][1]))
    hTotScatter_all[i][2].SetTitle(label)

cTotsScatter.Update()







#second cycle
print("second cycle to calculate amp walk correction")

results = plotTimeRes(df_list,config_list, vals_tot1, vals_tot2)
hCTR_all   = results[0]
gCTR_vs_TH = results[1]

pAmpWalk_all   = results[2]

cCTR = TCanvas ("cCTR", "cCTR", 600, 500)
hCTR_all[0][2].Draw()
hCTR_all[0][2].GetYaxis().SetRangeUser(1, hCTR_all[0][2].GetMaximum()*5)
hCTR_all[0][2].SetStats(0)
hCTR_all[0][2].GetXaxis().SetTitle("time1-time2 [ps]")
hCTR_all[0][2].GetYaxis().SetTitle("Counts")
legConfig = TLegend(0.15, 0.6, 0.5, 0.88)
for i in range(len(hCTR_all)):
    hCTR = hCTR_all[i]
    hCTR[2].SetLineColor(mycolors[i])
    hCTR[2].Draw("same")
    label = str("OV = "+str(hCTR[0])+ ", vth_{1} = "+str(hCTR[1]))
    legConfig.AddEntry(hCTR[2], label, "lpe")
    
legConfig.Draw()    
gPad.SetLogy()
cCTR.Update()




cAmpWalk = TCanvas ("cAmpWalk", "cAmpWalk", 600, 500)
pAmpWalk_all[0][2].Draw()
#pAmpWalk_all[0][2].GetYaxis().SetRangeUser(1, pAmpWalk_all[0][2].GetMaximum()*5)
pAmpWalk_all[0][2].SetStats(0)
pAmpWalk_all[0][2].GetXaxis().SetTitle("tot1/tot2")
pAmpWalk_all[0][2].GetYaxis().SetTitle("time1-time2 [ps]")
legConfig = TLegend(0.15, 0.6, 0.5, 0.88)
for i in range(len(pAmpWalk_all)):
    pAmpWalk = pAmpWalk_all[i]
    pAmpWalk[2].SetLineColor(mycolors[i])
    pAmpWalk[2].SetMarkerColor(mycolors[i])
    pAmpWalk[2].SetMarkerStyle(20)
    pAmpWalk[2].Draw("same")
    label = str("OV = "+str(pAmpWalk[0])+ ", vth_{1} = "+str(pAmpWalk[1]))
    legConfig.AddEntry(pAmpWalk[2], label, "lpe")
    
legConfig.Draw()    
#gPad.SetLogy()
cAmpWalk.Update()







print("third cycle to apply amp walk correction")

results = plotTimeResAmpCorr(df_list,config_list, vals_tot1, vals_tot2, pAmpWalk_all)
hCTR_AmpCorr_all   = results[0]
gCTR_AmpCorr_vs_TH = results[1]


cCTR_AmpCorr = TCanvas ("cCTR_AmpCorr", "cCTR_AmpCorr", 600, 500)
hCTR_AmpCorr_all[0][2].Draw()
hCTR_AmpCorr_all[0][2].GetYaxis().SetRangeUser(1, hCTR_AmpCorr_all[0][2].GetMaximum()*5)
hCTR_AmpCorr_all[0][2].SetStats(0)
hCTR_AmpCorr_all[0][2].GetXaxis().SetTitle("time1-time2 [ps]")
hCTR_AmpCorr_all[0][2].GetYaxis().SetTitle("Counts")
legConfig = TLegend(0.15, 0.6, 0.5, 0.88)
for i in range(len(hCTR_AmpCorr_all)):
    hCTR_AmpCorr = hCTR_AmpCorr_all[i]
    hCTR_AmpCorr[2].SetLineColor(mycolors[i])
    hCTR_AmpCorr[2].Draw("same")
    label = str("OV = "+str(hCTR_AmpCorr[0])+ ", vth_{1} = "+str(hCTR_AmpCorr[1]))
    legConfig.AddEntry(hCTR_AmpCorr[2], label, "lpe")
    
legConfig.Draw()    
gPad.SetLogy()
cCTR_AmpCorr.Update()


cCTR_AmpCorr_vs_TH = TCanvas ("cCTR_AmpCorr_vs_TH", "cCTR_AmpCorr_vs_TH", 600, 500)
gCTR_vs_TH.SetMarkerStyle(20)
gCTR_vs_TH.Draw("ALPE")
gCTR_vs_TH.GetXaxis().SetTitle("vth1 [dac]")
gCTR_vs_TH.GetYaxis().SetTitle("#sigma_{t}")
gCTR_vs_TH.SetMinimum(0)
gCTR_vs_TH.SetMaximum(60)

gCTR_AmpCorr_vs_TH.SetMarkerStyle(20)
gCTR_AmpCorr_vs_TH.SetMarkerColor(kGreen+1)
gCTR_AmpCorr_vs_TH.SetLineColor(kGreen+1)
gCTR_AmpCorr_vs_TH.Draw("same LPE")
gCTR_AmpCorr_vs_TH.GetXaxis().SetTitle("vth1 [dac]")
gCTR_AmpCorr_vs_TH.GetYaxis().SetTitle("#sigma_{t}")
gCTR_AmpCorr_vs_TH.SetMinimum(0)
gCTR_AmpCorr_vs_TH.SetMaximum(60)
cCTR_AmpCorr_vs_TH.Update()












outputFile = TFile(("./output/run"+str(sys.argv[1])+"_out.root"), "RECREATE")
outputFile.cd()
gCTR_vs_TH.SetName("gCTR_vs_TH")
gCTR_vs_TH.Write()
gCTR_AmpCorr_vs_TH.SetName("gCTR_AmpCorr_vs_TH")
gCTR_AmpCorr_vs_TH.Write()
outputFile.Write()
outputFile.Close()


input('Press ENTER to exit')


