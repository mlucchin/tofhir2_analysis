from root_pandas import read_root
import ROOT
from ROOT import TCanvas, TGraphErrors, TH1F, TH2F, TLegend, gStyle, gPad, TF1, TFile, TTree, TProfile
from ROOT import kGreen, kRed, kYellow, kBlue, kViolet, kCyan, kBlack, kOrange, kMagenta, kRainBow, kCopper, kSolar, kTemperatureMap, kOcean, kDarkBodyRadiator, kDeepSea
import sys
import numpy as np
import pandas as pd

mycolors=[kBlack, kRed+1, kBlue+1, kGreen+1, kYellow+1, kOrange+1, kViolet+1, kCyan+1,
                  kRed+2, kBlue+2, kGreen+2, kYellow+2, kOrange+2, kViolet+2, kCyan+2,
                  kRed,   kBlue,   kGreen,   kYellow,   kOrange,   kViolet,   kCyan]

for i in range(100):
    mycolors.append(kRainBow+i)

minTot = 4
maxTot = 10

def plotTots(my_df_list, config_list):
    

    hTot1_all = []
    hTot2_all = []
    hTotRatio_all = []
    hTotScatter_all = []
    vals_tot1 = []
    vals_tot2 = []
    
    for iConfig in range(len(config_list)):
        config = config_list[iConfig]
        ov   = float(config[0])
        vth1 = int(config[1]/10000)-1
    
        label       = str("_ov_"+str(ov)+ "_vth1_"+str(vth1))
        hTot1       = TH1F(("hTot1"+label), ("hTot1"+label), 1000, minTot, maxTot)
        hTot2       = TH1F(("hTot2"+label), ("hTot2"+label), 1000, minTot, maxTot)
        hTotRatio   = TH1F(("hTotRatio"+label), ("hTotRatio"+label), 1000, 0., 3.)
        hTotScatter = TH2F(("hTotScatter"+label), ("hTotScatter"+label), 1000, minTot, maxTot, 1000, minTot, maxTot)

        df_config = my_df_list[iConfig]                
        for i in range(len(df_config)):
            hTot1.Fill(df_config.tot1[i]/1000.)
            hTot2.Fill(df_config.tot2[i]/1000.)
            print("tot1 = ", df_config.tot1[i]/1000., " :: tot2 = ", df_config.tot2[i]/1000.)
            hTotRatio.Fill(df_config.tot1[i]/df_config.tot2[i])
            hTotScatter.Fill(df_config.tot1[i]/1000., df_config.tot2[i]/1000.)
        
     
        #mean = hTot1.GetMean()
        mean = hTot1.GetBinCenter(hTot1.GetMaximumBin())
        #rms  = hTot1.GetRMS()
        rms  = mean*0.05
        fitGaus = TF1("fitGaus", "gaus", mean-2*rms, mean+2*rms)
        fitGaus.SetLineColor(mycolors[iConfig])
        hTot1.Fit(fitGaus, "QR")
        mean1  = fitGaus.GetParameter(1)
        sigma1 = fitGaus.GetParameter(2)
        
        #mean = hTot2.GetMean()
        mean = hTot2.GetBinCenter(hTot2.GetMaximumBin())
        #rms  = hTot2.GetRMS()
        rms  = mean*0.05
        fitGaus = TF1("fitGaus", "gaus", mean-2*rms, mean+2*rms)
        fitGaus.SetLineColor(mycolors[iConfig])
        hTot2.Fit(fitGaus, "QR")
        mean2  = fitGaus.GetParameter(1)
        sigma2 = fitGaus.GetParameter(2)
        
        hTot1_all.append([ov, vth1 , hTot1])
        hTot2_all.append([ov, vth1 , hTot2])
        hTotRatio_all.append([ov, vth1 , hTotRatio])
        hTotScatter_all.append([ov, vth1 , hTotScatter])
        
        vals_tot1.append([ov, vth1 , mean1, sigma1])
        vals_tot2.append([ov, vth1 , mean2, sigma2])


    return(hTot1_all, hTot2_all, hTotRatio_all, hTotScatter_all, vals_tot1, vals_tot2)






def plotTimeRes(my_df_list, config_list, vals_tot1, vals_tot2):
    

    
    hCTR_all = []
    pAmpWalk_all = []
    gCTR_vs_OV = TGraphErrors()
    gCTR_vs_TH = TGraphErrors()
    ov_list = []
    th_list = []
    
    for iConfig in range(len(config_list)):
        config = config_list[iConfig]
        ov   = float(config[0])
        vth1 = int(config[1]/10000)-1
    
        label    = str("_ov_"+str(ov)+ "_vth1_"+str(vth1))
        hCTR     = TH1F(("hCTR"+label), ("hCTR"+label), 1000, -1000., 1000.)
        pAmpWalk = TProfile(("pAmpWalk"+label), ("pAmpWalk"+label), 1000, 0.7, 1.2)
    
        
        df_config  = my_df_list[iConfig]
        
        tot_mean1  = vals_tot1[iConfig][2]
        tot_sigma1 = vals_tot1[iConfig][3]
        tot_mean2  = vals_tot2[iConfig][2]
        tot_sigma2 = vals_tot2[iConfig][3]
        nsigma = 3
        
        
        #print("limits tot1 = ", tot_mean1, "+/-", tot_sigma1, " :: limits tot2 = ", tot_mean2, "+/-", tot_sigma2)
        df_config = df_config[  (df_config['tot1']/1000>tot_mean1-nsigma*tot_sigma1) & (df_config['tot1']/1000<tot_mean1+nsigma*tot_sigma1)
                              & (df_config['tot2']/1000>tot_mean2-nsigma*tot_sigma2) & (df_config['tot2']/1000<tot_mean2+nsigma*tot_sigma2) ]
        
        
        print ('length df_config after tot cuts: ', len(df_config))
        df_config.index = range (len(df_config))        
        delay = 0
        if (len(df_config)>0): delay = df_config.time1[0]-df_config.time2[0]
        
        for i in range(len(df_config)):            
            hCTR.Fill(df_config.time1[i]-df_config.time2[i]-delay)
            print("time1-time2 = ", df_config.time1[i]-df_config.time2[i]-delay, " (time1 = ", df_config.time1[i], ", time2 = ", df_config.time2[i], ")")
            pAmpWalk.Fill(df_config.tot1[i]/df_config.tot2[i], df_config.time1[i]-df_config.time2[i]-delay)
        
        
        #fit CTR
        mean = hCTR.GetMean()
        rms  = hCTR.GetRMS()
        fitGaus = TF1 ("fitGaus", "gaus", mean-rms*2, mean+rms*2)
        fitGaus.SetLineColor(mycolors[iConfig])
        hCTR.Fit(fitGaus, "QR")        
        sigma = fitGaus.GetParameter(2)
        gCTR_vs_TH.SetPoint(iConfig, vth1, sigma)
        gCTR_vs_TH.SetPointError(iConfig, 0, fitGaus.GetParError(2))
        
        #fit amp pAmpWalk
        minRatio = 0
        maxRatio = 10
        if ((tot_mean2+nsigma*tot_sigma2)!=0 and (tot_mean2-nsigma*tot_sigma2)!=0): 
            minRatio = (tot_mean1-nsigma*tot_sigma1)/(tot_mean2+nsigma*tot_sigma2)
            maxRatio = (tot_mean1+nsigma*tot_sigma1)/(tot_mean2-nsigma*tot_sigma2)
        print("minRatio=", minRatio, " :: maxRatio = ", maxRatio)
        
        funcAmpWalk = TF1 ("funcAmpWalk", "pol2", minRatio, maxRatio)
        funcAmpWalk.SetLineColor(mycolors[iConfig])
        
        pAmpWalk.GetXaxis().SetRangeUser(minRatio, maxRatio)
        #pAmpWalk.SetRange()
        pAmpWalk.Fit(funcAmpWalk, "QR")        
        
        
                
        #fill lists
        hCTR_all.append([ov, vth1 , hCTR, fitGaus])
        pAmpWalk_all.append([ov, vth1 , pAmpWalk, funcAmpWalk])
        
        


    return(hCTR_all, gCTR_vs_TH, pAmpWalk_all)




def plotTimeResAmpCorr(my_df_list, config_list, vals_tot1, vals_tot2, pAmpWalk_all):
    

    hCTR_AmpCorr_all = []    
    gCTR_AmpCorr_vs_OV = TGraphErrors()
    gCTR_AmpCorr_vs_TH = TGraphErrors()
    ov_list = []
    th_list = []
    
    for iConfig in range(len(config_list)):
        config = config_list[iConfig]
        ov   = float(config[0])
        vth1 = int(config[1]/10000)-1
        pAmpWalk = pAmpWalk_all[iConfig]
        funcAmpWalk = pAmpWalk[3]
    
        label        = str("_ov_"+str(ov)+ "_vth1_"+str(vth1))
        hCTR_AmpCorr = TH1F(("hCTR_AmpCorr_"+label), ("hCTR_AmpCorr_"+label), 1000, -1000., 1000.)
        
        
        df_config  = my_df_list[iConfig]
        
        tot_mean1  = vals_tot1[iConfig][2]
        tot_sigma1 = vals_tot1[iConfig][3]
        tot_mean2  = vals_tot2[iConfig][2]
        tot_sigma2 = vals_tot2[iConfig][3]
        nsigma = 3
        
        #print("limits tot1 = ", tot_mean1, "+/-", tot_sigma1, " :: limits tot2 = ", tot_mean2, "+/-", tot_sigma2)
        df_config = df_config[  (df_config['tot1']/1000>tot_mean1-nsigma*tot_sigma1) & (df_config['tot1']/1000<tot_mean1+nsigma*tot_sigma1)
                              & (df_config['tot2']/1000>tot_mean2-nsigma*tot_sigma2) & (df_config['tot2']/1000<tot_mean2+nsigma*tot_sigma2) ]
        
        
        print ('length df_config after tot cuts: ', len(df_config))
        
        df_config.index = range (len(df_config))        
        for i in range(len(df_config)):            
            hCTR_AmpCorr.Fill(df_config.time1[i]-df_config.time2[i] - funcAmpWalk.Eval(df_config.tot1[i]/df_config.tot2[i]) + funcAmpWalk.Eval(1))
        
        
        #fit CTR
        mean = hCTR_AmpCorr.GetMean()
        rms  = hCTR_AmpCorr.GetRMS()
        fitGaus = TF1 ("fitGaus", "gaus", mean-rms*2, mean+rms*2)
        fitGaus.SetLineColor(mycolors[iConfig])
        hCTR_AmpCorr.Fit(fitGaus, "QR")        
        sigma = fitGaus.GetParameter(2)
        gCTR_AmpCorr_vs_TH.SetPoint(iConfig, vth1, sigma)
        gCTR_AmpCorr_vs_TH.SetPointError(iConfig, 0, fitGaus.GetParError(2))                
                
        #fill lists
        hCTR_AmpCorr_all.append([ov, vth1 , hCTR_AmpCorr, fitGaus])
        
        


    return(hCTR_AmpCorr_all, gCTR_AmpCorr_vs_TH)
